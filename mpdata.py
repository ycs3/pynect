import multiprocessing as mp
import numpy
import ctypes

def mp2np(arr):
    _ctypes_to_numpy = {
            ctypes.c_char : numpy.int8,
            ctypes.c_wchar : numpy.int16,
            ctypes.c_byte : numpy.int8,
            ctypes.c_ubyte : numpy.uint8,
            ctypes.c_short : numpy.int16,
            ctypes.c_ushort : numpy.uint16,
            ctypes.c_int : numpy.int32,
            ctypes.c_uint : numpy.int32,
            ctypes.c_long : numpy.int32,
            ctypes.c_ulong : numpy.int32,
            ctypes.c_float : numpy.float32,
            ctypes.c_double : numpy.float64
    }
    address = arr._wrapper.get_address()
    size = arr._wrapper.get_size()
    dtype = _ctypes_to_numpy[arr._type_]
    class Dummy(object): pass
    d = Dummy()
    d.__array_interface__ = {
            'data': (address, False),
            'typestr': '|u1',
            'shape': (size,),
            'strides': None,
            'version': 3
    }
    return numpy.asarray(d).view(dtype=dtype)

struct2ctypes = {
        'c': ctypes.c_char,
        'char': ctypes.c_char,
        'int8': ctypes.c_char,
        'b': ctypes.c_char,
        'B': ctypes.c_ubyte,
        'uchar': ctypes.c_ubyte,
        'uint8': ctypes.c_ubyte,
        '?': ctypes.c_bool,
        'h': ctypes.c_short,
        'short': ctypes.c_short,
        'int16': ctypes.c_short,
        'H': ctypes.c_ushort,
        'ushort': ctypes.c_ushort,
        'uint16': ctypes.c_ushort,
        'i': ctypes.c_int,
        'int': ctypes.c_int,
        'I': ctypes.c_uint,
        'uint': ctypes.c_uint,
        'l': ctypes.c_long,
        'long': ctypes.c_long,
        'L': ctypes.c_ulong,
        'ulong': ctypes.c_ulong,
        'q': ctypes.c_longlong,
        'Q': ctypes.c_ulonglong,
        'f': ctypes.c_float,
        'float': ctypes.c_float,
        'd': ctypes.c_double,
        'double': ctypes.c_double,
        's': ctypes.c_char_p,
        'p': ctypes.c_char_p,
        'P': ctypes.c_void_p,
        }

def np2ptr(arr, c_type):
    data = arr.ravel()
    t = ctypes.POINTER(struct2ctypes[c_type])
    return data.ctypes.data_as(t)

class MPData(object):
    def __init__(self):
        self.data = {}
        self.mplock = mp.Lock()

    def _create_raw(self, c_type, dim, is_array=True):
        try:
            l = len(c_type)
            c_type = struct2ctypes[c_type]
        except TypeError:
            pass
        raw_item = {}
        total_pts = 1
        for d in dim:
            total_pts *= d
        raw_item['type'] = c_type
        raw_item['data'] = mp.RawArray(c_type, total_pts)
        raw_item['dim'] = dim
        raw_item['array'] = is_array
        return raw_item

    def add(self, key, c_type, dim=None, val=None):
        is_array = True
        if dim == None:
            dim = [1]
            is_array = False
        ret = self._create_raw(c_type, list(dim), is_array)
        self.data[key] = ret
        if not is_array and val != None:
            v = self._get_np(key)
            v[0] = val

    def put(self, key, val):
        if self.data[key]['array']:
            pass # can't put values in an array
        else:
            np_data = self._get_np(key)
            np_data[0] = val

    def get(self, key):
        np_data = self._get_np(key)
        if not self.data[key]['array']:
            return np_data[0]
        return np_data

    def ptr(self, key):
        np_data = self._get_np(key).ravel()
        t = ctypes.POINTER(self.data[key]['type'])
        return np_data.ctypes.data_as(t)

    def _get_np(self, key=None):
        ''' get a numpy 'view' of the data '''
        if key != None:
            return mp2np(self.data[key]['data']).reshape(self.data[key]['dim'])
        np_data = {}
        for key, mp_item in self.data.iteritems():
            np_item = mp2np(mp_item['data']).reshape(mp_item['dim'])
            np_data[key] = np_item
        return np_data
    
    def lock(self, blocking=True):
        return self.mplock.acquire(blocking)

    def unlock(self):
        self.mplock.release()

def _subprocess(lock_exit, mp_data):
    image = mp_data.get('image')
    j = 0
    try:
        while not lock_exit.acquire(False):
            if j >= 480:
                j = 0
            mp_data.lock()
            mp_data.put('user', j)
            for i in range(640):
                image[j][i][0] += 10
                image[j][i][1] += 10
                image[j][i][2] += 10
            mp_data.unlock()
            j += 1
    except KeyboardInterrupt:
        print "sub process interrupt"

def _main():
    import cv2

    lock_exit = mp.Lock()
    lock_exit.acquire()

    # init mp data
    mp_data = MPData()
    mp_data.add('image', 'B', (480, 640, 3))
    mp_data.add('user', ctypes.c_int)

    sub = mp.Process(target=_subprocess, args=(lock_exit, mp_data))
    sub.start()

    try:
        while not lock_exit.acquire(False):
            mp_data.lock()
            print mp_data.get('user')
            image = mp_data.get('image')
            cv2.imshow('OUT', image)
            cv2.waitKey(10)
            mp_data.unlock()
    except KeyboardInterrupt:
        print "main process interrupt"
        lock_exit.release()


if __name__ == '__main__':
    _main()
