#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libfreenect.h>

freenect_context *f_ctx;
freenect_device *f_dev;

// Flags-
// 0: ready to read from camera
// 1: ready to read depth or video
// 2: all ready to read both
uint16_t* g_depth = NULL;
int* g_depth_flag = NULL;

uint8_t*  g_video = NULL;
int* g_video_flag = NULL;
int g_video_high = 0;

uint32_t video_ts = 0;
uint32_t depth_ts = 0;

/*
 * Quick reference values from libfreenect.h, prefixed with FREENECT_
 * LOG_ERROR=1, LOG_DEBUG=5
 * LED_OFF=0, LED_GREEN=1, LED_RED=2, LED_YELLOW=3,
 * LED_BLINK_GREEN=4, LED_BLINK_RED_YELLOW=6
 * RESOLUTION_LOW=0, RESOLUTION_MEDIUM=1, RESOLUTION_HIGH=2 (1280x1024)
 * VIDEO_RGB=0, VIDEO_BAYER=1, VIDEO_IR_8BIT=2, VIDEO_IR_10BIT=3, ...
 * DEPTH_11BIT=0, DEPTH_10BIT=1, ..., DEPTH_REGISTERED=4, DEPTH_MM=5
 */

void
depth_cb(freenect_device *dev, void *v_depth, uint32_t timestamp)
{
    if(g_depth != NULL && *g_depth_flag != 2) {
        memcpy(g_depth, (const void*)v_depth, 480*640*sizeof(uint16_t));
        *g_depth_flag = 1;
        depth_ts = timestamp;
        printf("DEP: %ld\n", depth_ts);

        if(*g_depth_flag == 1 && *g_video_flag == 1) {
            *g_depth_flag = 2;
            *g_video_flag = 2;
        }
    }
}

void
video_cb(freenect_device *dev, void *rgb, uint32_t timestamp)
{
    if(g_video != NULL && *g_video_flag != 2) {
        if(g_video_high == 1)
            memcpy(g_video, (const void*)rgb, 1024*1280*3*sizeof(uint8_t));
        else
            memcpy(g_video, (const void*)rgb, 480*640*3*sizeof(uint8_t));
        *g_video_flag = 1;
        video_ts = timestamp;
        printf("VID: %d\n", video_ts);

        if(*g_depth_flag == 1 && *g_video_flag == 1) {
            *g_depth_flag = 2;
            *g_video_flag = 2;
        }
    }
}

int
start_capture(int log_level, int auto_exposure,
                         uint16_t* depth, int* depth_flag,
                          uint8_t* video, int* video_flag, int video_high)
{
    int n_dev;

    g_depth = depth;
    g_depth_flag = depth_flag;

    g_video = video;
    g_video_flag = video_flag;
    g_video_high = video_high;

    if(freenect_init(&f_ctx, NULL) < 0) {
        return -1;
    }

    freenect_set_log_level(f_ctx, log_level);
    freenect_select_subdevices(f_ctx,
      (freenect_device_flags)(FREENECT_DEVICE_MOTOR | FREENECT_DEVICE_CAMERA));

    n_dev = freenect_num_devices(f_ctx);
    if(n_dev < 1) {
        freenect_shutdown(f_ctx);
        return -2;
    }

    if(freenect_open_device(f_ctx, &f_dev, 0) < 0) {
        freenect_shutdown(f_ctx);
        return -3;
    }

    freenect_set_depth_callback(f_dev, depth_cb);
    freenect_set_video_callback(f_dev, video_cb);

    if(g_video_high == 1) {
        freenect_set_video_mode(f_dev,
                    freenect_find_video_mode(FREENECT_RESOLUTION_HIGH,
                                             FREENECT_VIDEO_RGB));
    }
    else {
        freenect_set_video_mode(f_dev,
                    freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM,
                                             FREENECT_VIDEO_RGB));
    }
    freenect_set_depth_mode(f_dev,
                freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM,
                                         FREENECT_DEPTH_REGISTERED));

    freenect_set_led(f_dev, LED_RED);

    //freenect_set_video_buffer(f_dev, g_video1);
    //freenect_set_depth_buffer(f_dev, g_depth1);
    freenect_start_depth(f_dev);
    freenect_start_video(f_dev);

    if(auto_exposure == 0) {
        freenect_set_flag(f_dev, FREENECT_AUTO_EXPOSURE, 0);
    }
    else {
        freenect_set_flag(f_dev, FREENECT_AUTO_EXPOSURE, 1);
    }
    freenect_set_flag(f_dev, FREENECT_AUTO_WHITE_BALANCE, 0);
    freenect_set_flag(f_dev, FREENECT_RAW_COLOR, 1);
    return n_dev;
}

int
process_events(void)
{
    int status;
    status = freenect_process_events(f_ctx);
    return status;
}

void
end_capture(void)
{
    g_depth = NULL;
    g_video = NULL;
    freenect_set_led(f_dev, LED_OFF);
    freenect_stop_depth(f_dev);
    freenect_stop_video(f_dev);
    freenect_close_device(f_dev);
    freenect_shutdown(f_ctx);
}
