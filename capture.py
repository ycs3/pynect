from nect import KinectCapture
import time
import sys
import cv2
import numpy
from os import path, makedirs

def save_frame(filename, video, depth):
    print filename
    sys.stdout.flush()
    cv2.imwrite(filename + "-rgb.pbm", video)
    cv2.imwrite(filename + "-depth.pbm", depth)

def main(dest):
    if path.isdir(dest):
        print "Warning:", dest, "already exists"
    else:
        print "Creating", dest
        try:
            makedirs(dest)
        except OSError:
            print "Error creating", dest
            exit(-1)
    raw_input("Press enter to begin recording")
    cap = KinectCapture()
    frame_no = 0
    try:
        while True:
            f, d = cap.getFrame()
            if f == None:
                continue

            curr_time = int(time.time() * 1000)
            filename = dest + "/ni4-" + ("%06d" % frame_no)\
                       + "-" + str(curr_time)
            save_frame(filename, f, d)
            cv2.imshow('VID', f)
            cv2.imshow('DEP', numpy.array(d, dtype=numpy.float)/3000.)
            cv2.waitKey(10)
            frame_no += 1
    except KeyboardInterrupt:
        pass
    cap.clean()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Usage: python", sys.argv[0], "[dest dir]"
        exit(-1)
    main(sys.argv[1])
