from win import Win3D, Input
import numpy
import atexit
import cv2
import scipy.weave as weave
import math
import glob

def color_depthmap(depth_data):
    depth = numpy.array((depth_data/5000.)*255, dtype=numpy.uint8)
    return depth

def image_resize(img, resize):
    ht, wt = img.shape[:2]
    if resize != None:
        if resize[0] != wt or resize[1] != ht:
            img = cv2.resize(img, (resize[0], resize[1]))
    return img

class RGBDStream(object):
    '''
    Read a jpg or png color image, png depth image set
    Read all jpg/png images from path

    self.color_file = (current color filename)
    self.depth_file = (current depth filename)
    self.frame_idx = (frame index)
    self.color = (color image)
    self.depth = (depth image)
    '''
    def __init__(self, path):
        self.color_file = None
        self.depth_file = None
        self.frame_idx = -1
        self.color = None
        self.depth = None

        self.path = path
        # read files from directory
        self.color_list = glob.glob(path + "/*-rgb.*")
        self.depth_list = glob.glob(path + "/*-depth.*")
        self.list_index = -1
        self.list_max = min(len(self.color_list), len(self.depth_list))

        self.color_list = sorted(self.color_list)
        self.depth_list = sorted(self.depth_list)

    def getFrame(self, step=1, resize=None):
        if self.list_index == self.list_max:
            return None, None

        self.list_index += step
        self.color_file = self.color_list[self.list_index]
        self.depth_file = self.depth_list[self.list_index]
        self.frame_idx += step

        self.color = cv2.imread(self.color_file)
        self.color = image_resize(self.color, resize)
        self.depth = self._getDepth(resize)
        return self.color, self.depth

    def _getDepth(self, resize=None):
        depth_raw = cv2.imread(self.depth_file, 2)
        depth_raw = image_resize(depth_raw, resize)
        return depth_raw

inline_depth2skel = r"""
void
_depth2skel(int i, int j, unsigned short d, int w, int h,\
           double* x, double* y, double* z)
{
    double ni, nj, fX, fY, fZ;

    ni = (double)i * (320./(double)w);
    nj = (double)j * (240./(double)h);
    fZ = (double)d/1000.;
    fX = (ni/320. - .5) * (3.501e-3 * fZ) * 320;
    fY = (.5 - nj/240.) * (3.501e-3 * fZ) * 240;

    *x = fX; *y = fY; *z = fZ;
}
"""

inline_conv = r"""
inline double _rad(double deg) { return deg*(3.14159265/180.); }

void
_conv(double x, double y, double z, double deg1, double deg2, double deg3,\
      double* ox, double* oy, double* oz)
{
    double c_a, s_a, c_b, s_b, c_c, s_c;
    double fx, fy, fz, nx, ny, nz;

    fx = x; fy = y; fz = z;
    // X (deg1, c_a, s_a)
    c_a = cos(_rad(deg1)); s_a = sin(_rad(deg1));
    nx = fx; ny = c_a*fy+s_a*fz; nz = -s_a*fy+c_a*fz;

    fx = nx; fy = ny; fz = nz;
    // Y (deg2, c_b, s_b)
    c_b = cos(_rad(deg2)); s_b = sin(_rad(deg2));
    nx = c_b*fx-s_b*fz; ny = fy; nz = s_b*fx+c_b*fz;

    fx = nx; fy = ny; fz = nz;
    // Z (deg3, c_c, s_c)
    c_c = cos(_rad(deg3)); s_c = sin(_rad(deg3));
    nx = c_c*fx+s_c*fy; ny = -s_c*fx+c_c*fy; nz = fz;

    *ox = nx; *oy = ny; *oz = nz;
}
"""

def skel2depth(x, y, z, w=320., h=240.):
    w, h = float(w), float(h)
    nX, nY, nZ = x, y, z
    dX = .5+nX*(285.63/nZ)/320.
    dY = .5-nY*(285.63/nZ)/240.
    dX, dY = dX * (w/320.), dY * (h/240.)
    return int(dX*320.), int(dY*240.)


def conv(ix, iy, iz, deg1, deg2, deg3):
    nx, ny, nz = ix, iy, iz

    c_c = math.cos(math.radians(deg3))
    s_c = math.sin(math.radians(deg3))
    fx, fy, fz = nx, ny, nz
    nx, ny, nz = c_c*fx+s_c*fy, -s_c*fx+c_c*fy, fz

    c_b = math.cos(math.radians(deg2))
    s_b = math.sin(math.radians(deg2))
    fx, fy, fz = nx, ny, nz
    nx, ny, nz = c_b*fx-s_b*fz, fy, s_b*fx+c_b*fz

    c_a = math.cos(math.radians(deg1))
    s_a = math.sin(math.radians(deg1))
    fx, fy, fz = nx, ny, nz
    nx, ny, nz = fx, c_a*fy+s_a*fz, -s_a*fy+c_a*fz
    return nx, ny, nz

def draw_scene(color, depth, trans, cutoff=-1, step=1, get_crop=False, show_output=True):
    h, w = color.shape[:2]
    da, db, dc, dx, dy, dz, x_min, x_max, y_min, z_max = trans
    new_color, new_3d, do_output, do_crop = None, None, 1, 0
    if get_crop == True:
        new_color = numpy.zeros(color.shape, dtype=color.dtype)
        new_3d = numpy.zeros((depth.shape[0], depth.shape[1], 3),\
                             dtype=numpy.double)
        do_crop = 1
    if show_output == False: do_output = 0
    code = r"""
    unsigned short d;
    double cr, cg, cb, ix, iy, iz, ox, oy, oz;

    if(do_output) {
        glBegin(GL_POINTS);
        glColor3f(1., 1., 1.);
    }

    for(int j = 0; j < h; j += step) {
        for(int i = 0; i < w; i += step) {
            d = depth[j*w+i];
            if((int)d == 0) { continue; }
            if((int)d < cutoff) { continue; }

            cr = (double)color[j*w*3+i*3+2]/255.;
            cg = (double)color[j*w*3+i*3+1]/255.;
            cb = (double)color[j*w*3+i*3+0]/255.;
            _depth2skel(i, j, d, w, h, &ix, &iy, &iz);
            _conv(ix, iy, iz, da, db, dc, &ox, &oy, &oz);
            if(-ox+dx < x_min) { continue; }
            if(-ox+dx > x_max) { continue; }
            if( oy+dy < y_min) { continue; }
            if( oz+dz > z_max) { continue; }
            if(do_output) {
                glColor3f(cr, cg, cb);
                glVertex3f(-ox+dx, oy+dy, oz+dz);
            }
            if(do_crop) {
                new_color[j*w*3+i*3+0] = color[j*w*3+i*3+0];
                new_color[j*w*3+i*3+1] = color[j*w*3+i*3+1];
                new_color[j*w*3+i*3+2] = color[j*w*3+i*3+2];

                new_3d[j*w*3+i*3+0] = -ox+dx;
                new_3d[j*w*3+i*3+1] =  oy+dy;
                new_3d[j*w*3+i*3+2] =  oz+dz;
            }
        }
    }

    if(do_output) {
        glEnd();
        glFlush();
    }
    """
    weave.inline(code,\
                 ['color', 'depth', 'w', 'h', 'cutoff', 'step',\
                  'da', 'db', 'dc', 'dx', 'dy', 'dz',\
                  'x_min', 'x_max', 'y_min', 'z_max',\
                  'new_color', 'new_3d', 'do_output', 'do_crop'],\
                 headers=['<GL/gl.h>'],\
                 support_code=inline_depth2skel+inline_conv)

    if get_crop:
        return new_color, new_3d

g_abc, g_xyz, g_cut = 1., .05, .05

def print_trans(trans):
    da, db, dc, dx, dy, dz, x_min, x_max, y_min, z_max = trans
    print "(abc:%6.3f)        |(xyz:%6.3f)           |(cut:%6.3f)"\
          % (g_abc, g_xyz, g_cut)
    print "%6.2f,%6.2f,%6.2f,%7.3f,%7.3f,%7.3f,%6.2f,%6.2f,%6.2f,%6.2f"\
          % (da, db, dc, dx, dy, dz, x_min, x_max, y_min, z_max)

def process_input(win, act, trans):
    global g_abc, g_xyz, g_cut
    da, db, dc, dx, dy, dz, x_min, x_max, y_min, z_max = trans
    for a in act:
        if a == "a_1": g_abc -= .1
        if a == "a_2": g_abc += .1
        if a == "a_3": g_xyz -= .005
        if a == "a_4": g_xyz += .005
        if a == "a_5": g_cut -= .01
        if a == "a_6": g_cut += .01

        if a == "a_q": da -= g_abc
        if a == "a_w": da += g_abc
        if a == "a_a": db += g_abc
        if a == "a_s": db -= g_abc
        if a == "a_z": dc += g_abc
        if a == "a_x": dc -= g_abc

        if a == "a_e": dx += g_xyz
        if a == "a_r": dx -= g_xyz
        if a == "a_d": dy += g_xyz
        if a == "a_f": dy -= g_xyz
        if a == "a_c": dz -= g_xyz
        if a == "a_v": dz += g_xyz

        if a == "a_u": x_min -= g_cut
        if a == "a_i": x_min += g_cut
        if a == "a_t": x_max -= g_cut
        if a == "a_y": x_max += g_cut
        if a == "a_g": y_min -= g_cut
        if a == "a_h": y_min += g_cut
        if a == "a_b": z_max -= g_cut
        if a == "a_n": z_max += g_cut

        if a == "a_8": win.rot_x = 0.; win.rot_y = 0.
        if a == "a_9": win.rot_x = -90.; win.rot_y = 0.
        if a == "a_0": win.rot_x = 0.; win.rot_y = -90.
    trans = [da, db, dc, dx, dy, dz, x_min, x_max, y_min, z_max]
    if len(act) > 0:
        print_trans(trans)
    return trans

def main(path=None):
    if path == None:
        return
    win = Win3D()
    inp = Input()
    stream = RGBDStream(path + "/")

    trans = [0., 0., 0., 0., 0., 0., -1., 1., -1., 1.]
    print_trans(trans)

    f, d = stream.getFrame()
    while True:
        f, d = stream.getFrame()
        if f == None:
            break
        print stream.color_file
        act = inp.event()
        win.event(act)
        trans = process_input(win, act, trans)
        win.clear()
        new_f, new_3d = draw_scene(f, d, trans, step=1, get_crop=True)
        cv2.imshow('RGB2', color_depthmap(d))
        cv2.imshow('RGB', new_f)
        cv2.waitKey(10)
        win.update(True)

if __name__ == '__main__':
    import sys
    try:
        main(sys.argv[1])
    except KeyboardInterrupt:
        pass
    except IndexError:
        pass
