# Wrapper for Kinect using libfreenect #

pynect is a python wrapper for reading RGB-D camera data (Kinect) created for compatibility with nimod (https://bitbucket.org/ycs3/nimod)

* nect.py: main class and example of live capture
* capture.py: saves RGB-D frames to external directory

libfreenect can be found here: https://github.com/OpenKinect/libfreenect