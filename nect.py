import ctypes
import multiprocessing as mp
from mpdata import MPData
import time
import cv2
import numpy

freenect = ctypes.cdll.LoadLibrary('./freenect_wrapper.so')
freenect.start_capture.argtypes = [ctypes.c_int, ctypes.c_int,\
                                      ctypes.POINTER(ctypes.c_uint16),\
                                      ctypes.POINTER(ctypes.c_int),\
                                      ctypes.POINTER(ctypes.c_uint8),\
                                      ctypes.POINTER(ctypes.c_int),\
                                      ctypes.c_int]

class KinectCapture(object):
    def __init__(self, video_high=False, auto_exposure=False, debug=False):
        self.log = 1
        if debug:
            # FREENECT_LOG_DEBUG=5
            self.log = 5
        self.num_kinects = 0
        self.data = MPData()
        self.video_high = video_high

        self.video, self.depth = None, None
        self.auto_exposure = auto_exposure
        if self.auto_exposure == True:
            self.auto_exposure_int = 1
        else:
            self.auto_exposure_int = 0

        self.data.add('depth', 'H', (480, 640))
        self.data.add('depth_flag', 'i', val=0)

        if video_high == True:
            dim = (1024, 1280, 3)
        else:
            dim = (480, 640, 3)
        self.data.add('video', 'B', dim)
        self.data.add('video_flag', 'i', val=0)

        self.exit_ = False
        self.cap = mp.Process(target=self._capture)
        self.cap.start()

    def _capture(self):
        depth = self.data.ptr('depth')
        depth_flag = self.data.ptr('depth_flag')

        video = self.data.ptr('video')
        video_flag = self.data.ptr('video_flag')

        if self.video_high:
            self.num_kinects = freenect.start_capture(self.log,\
                               self.auto_exposure_int,\
                               depth, depth_flag,\
                               video, video_flag, 1)
        else:
            self.num_kinects = freenect.start_capture(self.log,\
                               self.auto_exposure_int,\
                               depth, depth_flag,\
                               video, video_flag, 0)
        if self.num_kinects < 1:
            return
        try:
            while not self.exit_:
                ret = freenect.process_events()
                if ret < 0:
                    break
        except KeyboardInterrupt:
            pass
        freenect.end_capture()

    def getFrame(self):
        video, depth = None, None
        if self.data.get('video_flag') == 2\
                and self.data.get('depth_flag') == 2:
            self.video = self.data.get('video')
            self.depth = self.data.get('depth')
            self.data.put('video_flag', 0)
            self.data.put('depth_flag', 0)
            self.video = cv2.cvtColor(self.video, cv2.cv.CV_BGR2RGB)
        return self.video, self.depth

    def exit(self):
        self.exit_ = True
        self.cap.join()

    def clean(self):
        self.exit()

if __name__ == '__main__':
    cap = KinectCapture(False)
    try:
        while True:
            f, d = cap.getFrame()
            if f == None:
                continue
            cv2.imshow('VID', f)
            cv2.imshow('DEP', numpy.array(d, dtype=numpy.float)/3000.)
            k = cv2.waitKey(10)
    except KeyboardInterrupt:
        pass
    cap.clean()
